<?php

function register_navwalker(){
	require_once get_template_directory() . '/template-parts/navbar.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );

function mitema_agregar_css_js() {
  wp_enqueue_style('style', get_stylesheet_uri() );

  wp_enqueue_style('boostrap', get_template_directory_uri() . '/css/bootstrap.min.css');

  wp_enqueue_script('popper', 'https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js', array('jquery'), '2.9.2', true);
  wp_enqueue_script('bootsrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js', array('popper'), '5.0.2', true);
  
}
add_action( 'wp_enqueue_scripts', 'mitema_agregar_css_js' );

//Agregando Imagenes destacadas.
if(function_exists('add_theme_support') ){
   add_theme_support('post-thumbnails');
}

//Agregar sidebar
function mitema_widgets(){
  register_sidebar(array(
    'id' => 'widgets-publicidad',
    'name' => __('Mi sidebar'),
    'description' => ('Aqui va la publicidad'),
    'before_widget' => '<div class="card-body">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4> Aqui tenemos lo que necesitas',
    'after_title'   => '</h4><hr>',
  ));
}
add_action('widgets_init', 'mitema_widgets');

//Registrar mi menu
function mitema_register_my_menus() {
  register_nav_menus(
    array(
      'menu-principal' => __( 'Menu Superior' ),
     )
   );
 }
 add_action( 'init', 'mitema_register_my_menus' );

 //Agregar los comentarios
 function los_comentarios() {

  $numero_comentarios = wp_count_comments();
  return $numero_comentarios->total_comments;
  }
  
  add_shortcode('total_comentarios','los_comentarios');