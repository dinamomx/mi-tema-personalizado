<?php get_header(); ?>

<div class="container-fluid">
  <div class="container">
   <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="hero">
      <div class="hero-content">
        <h1><?php the_title(); ?></h1>
      </div>
      <div class="hero-background">
        <? the_post_thumbnail('large', ['class' => 'responsive']); ?>
      </div>
    </div>
    <div class="post-body etiquetas-flex">
      <aside class="share content-separacion">
        <h5>Compartir:</h5>
        <div class="iconos-compartir">
          <a class="compartir-icon" href="https://www.linkedin.com/sharing/share-offsite/?url=<?php the_permalink(); ?>" target="_blank">
            <svg  class="compartir-icon" width="23" height="23" viewBox="0 0 23 23" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M0.711186 7.41371H5.21546V22.3319H0.711186V7.41371ZM2.97804 0C4.42059 0 5.59818 1.2104 5.59818 2.69314C5.59818 4.17589 4.42059 5.38629 2.97804 5.38629C1.5355 5.38629 0.35791 4.17589 0.35791 2.69314C0.35791 1.2104 1.5355 0 2.97804 0Z" fill="currentColor"/>
              <path d="M8.07129 7.41371H12.3989V9.44113H12.4578C13.0466 8.26099 14.5186 7.02032 16.7266 7.02032C21.2897 7.02032 22.1435 10.1068 22.1435 14.1314V22.3016H17.6392V15.0392C17.6392 13.3144 17.6098 11.0752 15.284 11.0752C12.9288 11.0752 12.5756 12.9513 12.5756 14.8879V22.2714H8.07129V7.41371Z" fill="currentColor"/>
            </svg>
          </a>
          <a class="compartir-icon" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank">
            <svg class="compartir-icon" width="14" height="29" viewBox="0 0 14 29" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M3.39107 28.1116L8.86685 28.1419C8.86685 28.1419 8.89629 20.3953 8.89629 14.1012H12.959L13.4594 8.47282H9.10237V6.23357C9.10237 5.14421 9.80892 4.87187 10.3094 4.87187C10.8099 4.87187 13.3711 4.87187 13.3711 4.87187V0.0605238L9.19069 0C4.50978 0 3.42051 3.60095 3.42051 5.90071V8.44255H0.712056L0.682617 14.0709H3.42051C3.39107 20.4255 3.39107 28.1116 3.39107 28.1116Z" fill="currentColor"/>
            </svg>
          </a>
          <a class="compartir-icon" href="https://api.whatsapp.com/send?text=checa%20%este%20%articulo%20%<?php the_permalink(); ?>" target="_blank">
            <svg class="compartir-icon" width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
             <path fill-rule="evenodd" clip-rule="evenodd" d="M13.3067 24.1173C11.0988 24.1173 9.06743 23.4516 7.35992 22.3017L3.23837 23.6634L4.59259 19.5783C3.29725 17.7627 2.53181 15.5234 2.53181 13.1329C2.53181 12.7698 2.56125 12.4066 2.59069 12.0738C3.12061 6.50592 7.74264 2.11821 13.3362 2.11821C19.018 2.11821 23.6695 6.5967 24.0817 12.2553C24.1111 12.5277 24.1111 12.8303 24.1111 13.1026C24.1111 19.1849 19.2536 24.1173 13.3067 24.1173ZM26.1425 12.7698C25.9658 5.6889 20.284 0 13.3067 0C6.41785 0 0.794872 5.56786 0.500475 12.5277C0.500475 12.7092 0.500475 12.9211 0.500475 13.1026C0.500475 15.584 1.17759 17.8837 2.32574 19.8809L0 26.9012L7.12441 24.5712C8.94967 25.6 11.0693 26.2052 13.3362 26.2052C20.4311 26.2052 26.1719 20.3348 26.1719 13.1026C26.1425 13.0118 26.1425 12.8908 26.1425 12.7698Z" fill="currentColor"/>
             <path fill-rule="evenodd" clip-rule="evenodd" d="M19.1944 15.7655C18.8706 15.6142 17.3397 14.8274 17.0453 14.7064C16.7509 14.5854 16.5449 14.5551 16.3388 14.8577C16.1327 15.1906 15.5145 15.9168 15.3378 16.0984C15.1612 16.3102 14.9846 16.3404 14.6607 16.1891C14.3369 16.0378 13.3359 15.705 12.1289 14.5854C11.1868 13.7381 10.5686 12.679 10.392 12.3764C10.2153 12.0435 10.3625 11.8922 10.5392 11.7106C10.6864 11.5593 10.863 11.3475 11.0102 11.1357C11.0396 11.0752 11.0985 11.0449 11.128 10.9844C11.1868 10.8634 11.2457 10.7423 11.334 10.591C11.4518 10.3792 11.3929 10.1977 11.3046 10.0161C11.2163 9.86479 10.598 8.261 10.3331 7.62554C10.0681 6.99008 9.80318 7.08087 9.62654 7.08087C9.4499 7.08087 9.24382 7.05061 9.03775 7.05061C8.83167 7.05061 8.47839 7.14139 8.18399 7.44399C7.8896 7.77685 7.09473 8.53334 7.09473 10.1371C7.09473 10.5002 7.15361 10.8634 7.27136 11.2265C7.5952 12.3461 8.27231 13.2842 8.39007 13.4657C8.53727 13.6775 10.5686 17.0364 13.7775 18.3073C16.9865 19.5783 16.9865 19.1546 17.5458 19.0941C18.1346 19.0336 19.4005 18.3073 19.6655 17.5811C19.9304 16.8246 19.9304 16.1891 19.8421 16.0681C19.7244 16.0076 19.5183 15.9168 19.1944 15.7655Z" fill="currentColor"/>
            </svg>
          </a>
        </div>
      </aside>
      <main class="post-column">
        <div class="d-flex flex-row align-items-center content-separacion fecha-autor">
         <p class="small mb-0 mx-3 fecha-article">  <?php the_time('F,   Y'); ?></p>
         <p class="small mb-0 mx-3"> <img src="<?php  bloginfo('template_url'); ?>/images/icono-autor.png" alt="icono-autor" /> Autor: <?php the_author();?></p>
         
        </div>
        
        <?php the_content(); ?>
      </main> 
    </div>
    <?php  endwhile; endif; ?>
    <!--Fin Entrada-->
  <div class="widgets">
   <?php if ( is_active_sidebar( 'widgets-publicidad' ) ) : ?>
      <?php dynamic_sidebar( 'widgets-publicidad' ); ?>
   <?php else : ?>
    <!-- Time to add some widgets! -->
   <?php endif; ?>
  </div>
   </div>
</div>
  
    

<?php get_footer(); ?>
