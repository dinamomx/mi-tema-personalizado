<?php get_header(); ?>        
      <section id="bannerIndex" class="container-fluid">
	     <div class="banner-flex container" id="Inicio">
          <div class="columnas-banner">
             <h2 class="title-banner">
               ¡Bienvenido a nuestra página!  Nos da mucho gusto que te estés tomando el tiempo para visitarnos.
              </h2>
			        <p class="text-banner">
                Queremos atenderte lo mejor posible y hacer que tu visita sea productiva.
              </p>
			       <a class="boton-banner" href="#Servicios">Ver más</a>
          </div>
          <div class="columnas-banner">
            <div>
               <img class="imagen-header" src="<?php  bloginfo('template_url'); ?>/images/image-header.png" alt="">
            </div>
          </div>
		    </div>
      </section>

      <section>
        <div class="container-fluid">
         <div class="contenedor-nosotros container" id="Nosotros">
           <div class="columna-imagen">
             <img class="corporativo" src="<?php  bloginfo('template_url'); ?>/images/imagen-oscar.jpg" alt="">
           </div>
           <div class="columna-text">
             <div class="nosotros-parrafo">
                <h3 class="title-objetivo">
                  <img src="<?php  bloginfo('template_url'); ?>/images/icono-nosotros.png" alt="">
                  Nosotros
                </h3>
                <p class="parrafo-objetivos">
                  El mundo de los negocios B2B demanda de sus proveedores una alta integración estratégica, por lo 
                  que se hace mandatorio, para el éxito de las empresas, desarrollar mas competencias de Venta 
                  Consultiva y hacerse expertos en el ecosistema de sus Clientes para poder abrir 
                  conversaciones de valores para todos los involucrados.
                </p>
                <p class="parrafo-objetivos">
                 Con más de 30 años de interacción y colaboración exitosa con Multinacionales de clase mundial,
                 nosotros ayudamos a nuestros clientes a descubrir, diseñar e implementar su propio Sistema de Ventas,
                 tomando como base sus recursos y características únicas, además de sus objetivos de negocio de corto,
                 mediano y largo plazo.
                </p>
                <p class="parrafo-objetivos">
                 Llamanos hoy mismo o agenda una cita para entender tu situación
                 y objetivos, para que juntos definamos la mejor forma de colaboración.
                </p>
                <div class="mision-vision">
                <div class="columna-objetivos">
                  <h3 class="title-objetivos">
                    <img src="<?php  bloginfo('template_url'); ?>/images/mision-icono.png" alt="">
                    Misión
                  </h3>
                  <p class="text-content">
                    Ayudamos a nuestros clientes a desarrollar e implementar su propio sistema de ventas para
                    mejorar sus resultados de negocio.
                  </p>
                </div>
                <div class="columna-objetivos">
                  <h3 class="title-objetivos">
                    <img src="<?php  bloginfo('template_url'); ?>/images/vision-icono.png" alt="">
                     Visión
                  </h3>
                  <p class="text-content">
                   Contribuimos al crecimiento y profesionalización de las empresas y empresarios mexicanos,
                   a partir del desarrollo de sus competencias comerciales, específicamente del enfoque
                   consultivo en las ventas.
                  </p>
                </div>
               </div>
             </div>
           </div>
         </div>
        </div>
      </section>
      <section id="Servicios" class="container-fluid">
        <div class="content-servicios container"> 
           <p class="introducion-servicios">
             Nuestro principal servicio es el Coaching en ventas B2B para empresarios,
             sin embargo, en los últimos meses nos han pedido que diseñemos talleres
             de Venta Consultiva para Vendedores y Gerentes, los cuales puedes ver
             a detalle en esta sección, para que elijas y te registres en el que
             más te interese en este momento de tu carrera.
           </p>
           <div class="consultoria-coaching">
             <div class="columna-coaching">
               <h2 class="titulo-seccion">
                 Servicios
               </h2>
               <h3 class="subtitulo-seccion">
                 Coaching de ventas B2B para Empresarios:
               </h3>
               <p class="text-servicios">
                 A través de sesiones de Coaching, les ayudó a identificar sus buenas prácticas
                 comerciales, así como sus áreas de oportunidad, con el objetivo de descubrir
                 su mejor sistema de ventas para implementarlo y automatizarlo en
                 su organización.
               </p>
               <p class="text-servicios">
                Contribuyendo así al crecimiento y desarrollo sustentable del negocio y sus colaboradores.
               </p>
               <a id="abrir" class="boton-contacto " href="#Contacto">Contáctame</a>
             </div>
             <div class="columna-coaching">
                <img class="imagen-equipo" src="<?php  bloginfo('template_url'); ?>/images/coaching.png" alt="Asesoramiento">
             </div>
           </div>
           <div class="consultoria-coaching">
             <div class="columna-coaching">
               <img class="imagen-equipo" src="<?php  bloginfo('template_url'); ?>/images/curso-elvendedor.png" alt="Asesoramiento">
             </div>
             <div class="columna-coaching">
               <h3 class="subtitulo-seccion">
                 Taller “El Vendedor Consultor”
               </h3>
               <p class="text-servicios">
                 Con una dinámica de sesiones de trabajo participativo y colaborativo,
                 ayudo a los asistentes a conocer y entender conceptos y herramientas
                 relativas a las competencias clave para que sigan avanzando en su
                 profesionalización como Vendedores Consultores.  El Taller consiste en
                 5 sesiones semanales con una duración de 2 horas. cada una, el participante
                 trabaja también por su cuenta con tareas a desarrollar e implementar durante
                 los días entre sesión y sesión.  El cupo máximo para este taller es de
                 8 participantes, los horarios son flexibles y los define cada grupo.
               </p>
             </div>
           </div>
           <div class="consultoria-coaching">
             <div class="columna-coaching">
               <h3 class="subtitulo-seccion">
                 Taller “El Gerente Coach de Ventas”
               </h3>
               <p class="text-servicios">
                 Con una dinámica de sesiones de trabajo participativo y colaborativo,
                 ayudo a los asistentes a conocer y entender conceptos y herramientas
                 relativas a las comerciales clave para que sigan avanzando en la
                 profesionalización de sus organizaciones, 
                 El Taller consiste en 5 sesiones semanales con una duración de 2
                 horas cada una, además los participantes también trabajan por su
                 cuenta con tareas a desarrollar e implementar durante los días entre
                 sesión y sesión.  El cupo máximo para este taller es de 4 participantes.
                 Los horarios son flexibles y los definen los participantes
               </p>
             </div>
             <div class="columna-coaching">
               <img class="imagen-equipo" src="<?php  bloginfo('template_url'); ?>/images/curso-gerente.png" alt="Asesoramiento">
             </div>
           </div>
           <div class="mis-libros">
             <h2 class="titulo-seccion align-text">
               Mis Libros
             </h2>
             <p class="text-servicios">
               Estos trabajos resumen y recopilan herramientas y experiencias
               vividas durante los últimos 30 años, también vas a encontrar tips
               y conceptos que te ayudarán a visualizar y complementar tu formación
               como Vendedor Consultor y como gerente Coach de Ventas. 
             </p>
             <div class="libros-destacados">
                <div class="columna-libros">
                  <div class="libro-imagen">
                    <img src="<?php  bloginfo('template_url'); ?>/images/vendedor.png" alt="Los cuatro pilares del vendedor consultor">
                  </div>
                  <div class="descripcion-libro">
                  <h3 class="subtitulo-seccion">
                     Los cuatro pilares del vendedor consultor
                   </h3>
                   <p class="text-servicios">
                     Esta obra recopila la experiencia de más de 22 años y a
                     manera de resumen ofrece herramientas practicas para pasar
                     de ser un vendedor a un vendedor consultor exitoso.
                   </p>
                   <a class="boton-banner" target="_blank" href="https://www.amazon.com/Los-Pilares-Vendedor-Consultor-Spanish-ebook/dp/B0793PPKZ2/ref=sr_1_fkmr1_1?dchild=1&keywords=Los+cuatro+pilares+del+vendedor+consultor&qid=1625771482&sr=8-1-fkmr1">Comprar</a>
                  </div>
                </div>
                <div class="columna-libros">
                  <div class="libro-imagen">
                     <img src="<?php  bloginfo('template_url'); ?>/images/coachventas.png" alt="Los cuatro pilares del Gerente Coach de Ventas">
                  </div>
                  <div class="descripcion-libro">
                     <h3 class="subtitulo-seccion">
                       Los cuatro pilares del Gerente Coach de Ventas
                     </h3>
                     <p class="text-servicios">
                       Esta obra recopila la experiencia de más de 22 años, 
                       colaborando con multinacionales en el segmento industrial B2B 
                       y un conjunto de conceptos y herramientas 
                       para el desarrollo.
                     </p>
                     <a class="boton-banner" target="_blank" href="https://www.amazon.com/Pilares-Gerente-Coach-Ventas-Spanish-ebook/dp/B07FRCM1LH/ref=sr_1_fkmr1_1?dchild=1&keywords=Los+cuatro+pilares+del+Gerente+Coach+de+Ventas&qid=1625771555&sr=8-1-fkmr1">Comprar</a>
                  </div>
                </div>
             </div>
           </div>
        </div>
      </section>
      <section class="container-fluid">
         <div class="seccion-blog container">
          <!--Entradas-->
          <h2 class="align-text title-green"> 
            El Vendedor Consultor 
          </h2>
          <p class="text-consultor align-text">
           En las diferentes entregas de este blog podrás encontrar enfoques,
           opiniones, comentarios y discusiones, de temas relativos a las Ventas Consultivas,
           te invito a que revises el índice de entradas, son solo 5 a 10 minutos de lectura
           y reflexión. ¡Déjanos tu opinión! Para que nos ayudes a enriquecer aún más esta sección.
          </p>
           <div id="contentcards" class="">
             <!--Entrada-->
                <?php query_posts( array(
                  'posts_per_page' => 3,
                )); 
                
                if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
               <div id="" class="card-body">
                 <? the_post_thumbnail('medium', ['class' => 'modernizr-of image cover image-card', 'loading' => 'lazy']); ?>
                  <p class="small  boton-fecha"> <?php the_time('F , Y'); ?></p>
                    <div class="card-content">
                      <a class="title-articulos" href="<?php the_permalink(); ?>"> <!-- dirige a single.html--> 
                        <h2> 
                          <?php the_title(); ?>
                        </h2>
                      </a>
                      <div class="d-flex">
                        <p class="small ">
                        <img src="<?php  bloginfo('template_url'); ?>/images/icono-autor.png" alt="icono-autor" /> Autor: <?php the_author();?>
                        </p>
                        <p class="small">
                          <img src="<?php bloginfo('template_url'); ?>/images/comentarios.png" alt=""> Comentarios;
                        </p>
                      </div>
                      <!--<p class="small"> Categoria: <?php the_category( '/',);  ?> Etiquetas: <?php the_tags('', '/', ''); ?> </p> -->
                      
                      <?php the_excerpt(); ?>
                    </div>
                  </div>
                <?php  endwhile; endif; ?>
             <!--Fin Entrada-->

           </div>
           <!--Fin de las entradas-->
         </div>         
        </div>
       <!--Fin del blog-->
    </section>
    <section id="Testimonios" class="container-fluid">
      <div class="section-testimonio container">
        <h2 class="align-text title-green"> 
          Testimonios
        </h2>
        <p class="text-consultor align-text">
          Te comparto mi experiencía a través de otros profesionales
        </p>
        <div style="width: 100%; padding: 1rem 1rem 1rem 1rem; display: flex; justify-content: center; align-items: center;">
        <div class="splide" style="padding: 1rem 1rem 1rem 1rem; width: 100%">
        <div class="splide__arrows">
          <button class="splide__arrow splide__arrow--prev" type="button" aria-controls="splide01-track" aria-label="Previous slide" disabled="">
            <img src="<?php  bloginfo('template_url'); ?>/images/prev.png" alt="">
          </button>
          <button class="splide__arrow splide__arrow--next" type="button" aria-controls="splide01-track" aria-label="Next slide">
            <img src="<?php  bloginfo('template_url'); ?>/images/next.png" alt="">
          </button>
        </div>
	        <div class="splide__track">
		        <ul class="splide__list">
			       <li class="splide__slide">
               <div class="contenedor-testimonio">
                  <div class="encabezado-testimonio">
                     <img class="imagen-redondeada" src="<?php  bloginfo('template_url'); ?>/images/monserrat.png" alt="">
                     <h4 class="nombre-testimonio">
                       Monserrat Del Villar
                     </h4>
                     <p class="profesion">
                       Especialista en Desarrollo Organizacional en Balluff de Mexico
                     </p>
                  </div>
                  <div class="content-testimonio">
                    <p class="descripcion-testimonio">
                      "Oscar es un excelente compañero de trabajo, orientado a objetivos
                      y con amplias habilidades comerciales. Siempre abierto y dispuesto
                      a colaborar para el cumplimiento de los objetivos organizacionales
                      y un aliado para impulsar las iniciativas relacionadas con la
                      atracción, identificación, desarrollo y retención de talento".
                      (Tomado de Linkedin)
                      <br>
                      Colaboramos en Balluff de México por +6 años y somos partners en la actualidad.
                    </p>
                  </div>
               </div>
              </li>
			       <li class="splide__slide">
               <div class="contenedor-testimonio">
                  <div class="encabezado-testimonio">
                     <img class="imagen-redondeada" src="<?php  bloginfo('template_url'); ?>/images/everardo.png" alt="">
                     <h4 class="nombre-testimonio">
                       Everardo Bocanegra
                     </h4>
                     <p class="profesion">
                       BDM En Thyssenkrupp
                     </p>
                  </div>
                  <div class="content-testimonio">
                    <p class="descripcion-testimonio">
                     "Óscar Albarrán es un excelente ser humano y líder, enfocado a resultados
                     y siempre consciente de las necesidades de su equipo, un profesional
                     estratega que busca que su equipo mantenga un balance en su vida laboral
                     y personal, siendo congruente. Un experto en ventas y coach, que sin duda
                     obtiene los resultados esperados en donde se desarrolle" (tomado de Linkedin)
                     <br>
                     Colaboramos en Balluff varios años.
                    </p>
                  </div>
                </div>
              </li>
			        <li class="splide__slide">
                <div class="contenedor-testimonio">
                  <div class="encabezado-testimonio">
                     <img class="imagen-redondeada" src="<?php  bloginfo('template_url'); ?>/images/alexis.png" alt="">
                     <h4 class="nombre-testimonio">
                       Alexis Herreria Valero
                     </h4>
                     <p class="profesion">
                       Socio fundador de Equipos de Valor
                     </p>
                  </div>
                  <div class="content-testimonio">
                    <p class="descripcion-testimonio">
                     "Óscar Albarrán es un alto directivo comercial con gran foco en resultados
                     y cómo lograrlos en equipo. Lo recomiendo ampliamente como profesional y
                     como persona por sus valores y su congruencia" (Tomado de Linkedin)
                     <br>
                     Colaboramos en Sika Mexicana por +6 años como miembros  del Equipo Gerencial.
                    </p>
                  </div>
               </div>
              </li>
              <li class="splide__slide">
               <div class="contenedor-testimonio">
                  <div class="encabezado-testimonio">
                     <img class="imagen-redondeada" src="<?php  bloginfo('template_url'); ?>/images/antonio.png" alt="">
                     <h4 class="nombre-testimonio">
                       Antonio Balbín
                     </h4>
                     <p class="profesion">
                       CEO en IAS Automation
                     </p>
                  </div>
                  <div class="content-testimonio">
                    <p class="descripcion-testimonio">
                     "Oscar se destaca por su alta sensibilidad a las necesidades de sus clientes, trabajo en equipo,
                     desarrollo de personal y estrategias. Persona muy confiable y de alta integridad.
                     Orientado a la alta adaptabilidad de los cambios del mercado en México. Liderazgo y empuje".(Tomado de Linkedin)
                     <br>
                     Colaboramos por +6 años, relacionados con Balluff de Mexico.
                    </p>
                  </div>
                </div>
              </li>
              <li class="splide__slide">
               <div class="contenedor-testimonio">
                  <div class="encabezado-testimonio">
                     <img class="imagen-redondeada" src="<?php  bloginfo('template_url'); ?>/images/jaime.png" alt="">
                     <h4 class="nombre-testimonio">
                       Jaime Navarro
                     </h4>
                     <p class="profesion">
                       Gerente De Tecnologías en Impresión 3D en Éter Tecnologías de Manufactura
                     </p>
                  </div>
                  <div class="content-testimonio">
                    <p class="descripcion-testimonio">
                     "Oscar se caracteriza por ser práctico e incluir a las partes involucradas
                     de manera inclusiva, orientado en resolver en el menor tiempo posible
                     cuidando los recursos humanos y materiales, haciendo conciencia en su
                     equipo directo e indirecto para satisfacer las necesidades de los
                     clientes en un esquema ganar-ganar”
                     <br>
                     Colaboramos en Balluff de México por +6 años.
                    </p>
                  </div>
                </div>
              </li>
              <li class="splide__slide">
               <div class="contenedor-testimonio">
                  <div class="encabezado-testimonio">
                     <img class="imagen-redondeada" src="<?php  bloginfo('template_url'); ?>/images/angel.png" alt="">
                     <h4 class="nombre-testimonio">
                     Angel Tello
                     </h4>
                     <p class="profesion">
                       Gerente de Terget Market en Sika Mexicana
                     </p>
                  </div>
                  <div class="content-testimonio">
                    <p class="descripcion-testimonio">
                     Oscar is an excelent sales manager. Even when he is focusing on the results,
                     always is aware of his team's needs, therefore he is able to achieve the
                     objectives and overcome any problematic situation successfully
                     <br>
                     Colaboramos en Sika Mexicana por +6 años
                    </p>
                  </div>
                </div>
              </li>
		        </ul>
	        </div>
         </div>
        </div>
    </section>
    <section class="container-fluid">
       <div class="section-socios container">
         <h2 class="align-text title-green"> 
           Aliados Estratégicos
         </h2>
         <p class="text-consultor align-text">
           En esta sección podrás encontrar a nuestros aliados estratégicos,
           quienes te pueden ayudar en temas relativos a las áreas de soporte,
           para que el crecimiento de tu organización sea integral y más efectivo. 
         </p>
         <div class="slide-socios">
            <div class="testimonials-carousel-wrap">
               <div class="listining-carousel-button listing-carousel-button-prev">
                 <img src="<?php  bloginfo('template_url'); ?>/images/prev.png" alt="">
               </div>
               <div class="listining-carousel-button listing-carousel-button-next">
                 <img src="<?php  bloginfo('template_url'); ?>/images/next.png" alt="">
               </div>
             </div>
             <div class="testimonials-carousel">
               <div class="swiper-container">
                 <div class="swiper-wrapper">
                   <div class="swiper-slide">
                     <div class="testi-item">
                       <div class="testi-avatar">
                         <img src="<?php  bloginfo('template_url'); ?>/images/logo-dinamo.png" alt="">
                       </div>
                       <div class="testimonials-text">
                          <p class="text-socio">
                            Agencia especializada en Marketing Digital y diseño web.
                            Contamos con un equipo de especialistas en diferentes áreas,
                            que nos permite adaptarnos a las necesidades de tu proyecto.
                          </p>
                          <a href="" class="text-link link-contacto">
                           Contacto: hola@wdinamo.com
                          </a>
                       </div>
                     </div>
                   </div>

                   <!-- second slide -->
                   <div class="swiper-slide">
                     <div class="testi-item">
                       <div class="testi-avatar">
                         <img src="<?php  bloginfo('template_url'); ?>/images/logo-backstay.png" alt="">
                       </div>
                       <div class="testimonials-text">
                          <p class="text-socio">
                            Especialistas en seguros de crédito y fianzas, para que no aumentes
                            tu riesgo de cartera vencida y no dejes de vender a nuevos clientes.
                          </p>
                          <a href="" class="text-link link-contacto">
                           Contacto: sin correo
                          </a>
                       </div>
                     </div>
                   </div>
                   <!-- third slide -->
                   <div class="swiper-slide">
                     <div class="testi-item">
                       <div class="testi-avatar">
                         <img src="<?php  bloginfo('template_url'); ?>/images/logo-agregando.png" alt="">
                       </div>
                       <div class="testimonials-text">
                          <p class="text-socio">
                           Consultoría-Implementación de Nom 035 y DO. Encuestas de clima 
                           laboral y satisfacción al cliente.
                          </p>
                          <a href="" class="text-link link-contacto">
                           Contacto: www.agregandovalor.com
                          </a>
                       </div>
                     </div>
                   </div>
                   <!-- four slide -->
                   <div class="swiper-slide">
                     <div class="testi-item">
                       <div class="testi-avatar">
                         <img src="<?php  bloginfo('template_url'); ?>/images/logo-osi.png" alt="">
                       </div>
                       <div class="testimonials-text">
                          <p class="text-socio">
                            Especialista en Tests psicométricos para vendedores, 
                            ejecutivos, gerentes y directivos comerciales B2B.
                          </p>
                          <a href="" class="text-link link-contacto">
                           Contacto: socorro.chavolla@gmail.com
                          </a>
                       </div>
                     </div>
                   </div>
                   <!--five slide -->
                   <div class="swiper-slide">
                     <div class="testi-item">
                       <div class="testi-avatar">
                         <img src="<?php  bloginfo('template_url'); ?>/images/logo-rom.png" alt="">
                       </div>
                       <div class="testimonials-text">
                          <p class="text-socio">
                            Especialistas en asesoría y auditorías fiscal, contable y comercial.
                          </p>
                          <a href="" class="text-link link-contacto">
                           Contacto: www.romyasa.com
                          </a>
                       </div>
                     </div>
                   </div>
                   <!--six slide -->
                   <div class="swiper-slide">
                     <div class="testi-item">
                       <div class="testi-avatar">
                         <img src="<?php  bloginfo('template_url'); ?>/images/logo-sales.png" alt="">
                       </div>
                       <div class="testimonials-text">
                          <p class="text-socio">
                            CRM customizable para que automatices tu propio proceso de ventas.
                          </p>
                          <a href="" class="text-link link-contacto">
                           Contacto: alondra.vera@salesup.com.mx
                          </a>
                       </div>
                     </div>
                   </div>
                   <!--Seven Slide -->
                   <div class="swiper-slide">
                     <div class="testi-item">
                       <div class="testi-avatar">
                         <img src="<?php  bloginfo('template_url'); ?>/images/logo-berry.png" alt="">
                       </div>
                       <div class="testimonials-text">
                          <p class="text-socio">
                           Desarrollo de aplicaciones móviles para iOS y Android y Sistemas a
                           la medida de forma escalable y con la más alta calidad según
                           los estándares de la industria del Software.
                          </p>
                          <a href="" class="text-link link-contacto">
                           Contacto: contact@strappberry.com
                          </a>
                       </div>
                     </div>
                   </div>
                   <!--Eight slide -->
                   <div class="swiper-slide">
                     <div class="testi-item">
                       <div class="testi-avatar">
                         <img src="<?php  bloginfo('template_url'); ?>/images/logo-elsa.png" alt="">
                       </div>
                       <div class="testimonials-text">
                          <p class="text-socio">
                            Consultora Coach, especialista en desarrollo de competencias
                            suaves o soft skills, para ejecutivos y
                            directivos comerciales.
                          </p>
                          <a href="" class="text-link link-contacto">
                           Contacto: elsatortolero2016@gmail.com
                          </a>
                       </div>
                     </div>
                   </div>
                   <!--fin seccion socios -->
                 </div>
                 <div class="tc-pagination"></div>
               </div>
             </div>
           </div>
       </div>
     </section>

<?php get_footer(); ?>
