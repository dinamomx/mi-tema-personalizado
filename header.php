<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image" content="https://dev.elvendedorconsultor.com/og-oscar.jpg"> 
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;400;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.8/swiper-bundle.min.css"/>
     <link href="<?php bloginfo('template_url'); ?>/css/sitio.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    
    
    <title><?php wp_title(''); ?></title>
    <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
    <style type="text/css">
	  #mc_embed_signup{
      background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; 
    }
    </style>

    <?php wp_head();?>
  </head>
  <body>
    
    <!--Menu -->
    <nav class="container-fluid navbar navbar-expand-lg fondo-menu navbar-dark bg-dark">
     <div class="container">
        <a class="navbar-brand" href="<?php echo esc_url( home_url('/')); ?>">
          <img class="imagen-menu" src="<?php  bloginfo('template_url'); ?>/images/logo-menu.png" alt="Logo el Consultor">
        </a>  
        
       <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
       </button>
       

       <?php
        wp_nav_menu( array(
            'theme_location'    => 'menu-principal',
            'depth'             => 2,
            'container'         => 'div',
            'container_class'   => 'collapse navbar-collapse',
            'container_id'      => 'navbarText',
            'menu_class'        => 'nav navbar-nav ms-auto mb-2 mb-lg-0',
            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
            'walker'            => new WP_Bootstrap_Navwalker(),
        ) );
        ?>

        <img class="d-none" src="<?php  bloginfo('template_url'); ?>/images/og-oscar.jpg" alt="imagen para compartir el sitio">
     </div>
    </nav>
    <!--fin del menu -->