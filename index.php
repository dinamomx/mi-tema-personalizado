<?php

get_header() 

?>
<div class="container-fluid" style="background-color: #F9F9F9">
  <div class="container">
    <div class="hero">
     <div class="hero-content">
       <h1>El Vendedor Consultor</h1>
       <p>
         ¡Bienvenido a nuestro blog! Encuentra en esta sección herramientas que te ayuden a entender el mundo de las ventas y la consultoría.
       </p>
     </div>
     <div class="hero-background">
       <img src="<?php  bloginfo('template_url'); ?>/images/vendedor-header.png" alt="Fondo ilustrativo">
     </div>
    </div>
  </div>
</div>
<main class="container">
  <div id="contentcards">
    <? while (have_posts()) : the_post() ?>
     <div id="" class="card-body">
      <? the_post_thumbnail('medium', ['class' => 'modernizr-of image cover image-card', 'loading' => 'lazy']); ?>
      <p class="small  boton-fecha"> <?php the_time('F , Y'); ?></p>

      <div class="card-content">
      
        <a class="title-articulos" href="<?php the_permalink(); ?>">
        <h2><?php the_title(); ?></h2>
        </a>
        <div class="d-flex">
        <p class="small ">
          <img src="<?php  bloginfo('template_url'); ?>/images/icono-autor.png" alt="icono-autor" /> Autor: <?php the_author();?>
          </p>
          <p class="small">
            <img src="<?php bloginfo('template_url'); ?>/images/comentarios.png" alt=""> Comentarios;
          </p>
      </div>
        <?php the_excerpt(); ?>
      </div>
     </div>
    <? endwhile; ?>
  </div>
</main>

<? get_footer() ?>