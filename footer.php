<div class="container-fluid">
<div id="mc_embed_signup" class="container mt-3 mb-3">
<form action="https://elvendedorconsultor.us5.list-manage.com/subscribe/post?u=8a7031a61506935dea28f505b&amp;id=dba19e003b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	<h2>Suscríbete a nuestro Newsletter</h2>
<div class="mc-field-group">
	<label for="mce-EMAIL">Correo Electrónico(requerido)  <span class="asterisk">*</span>
</label>
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</div>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none; color: #26B78A;"></div>
		<div class="response" id="mce-success-response" style="display:none; color: #26B78A;"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_8a7031a61506935dea28f505b_dba19e003b" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Suscríbete" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>
</form>
</div>
</div>

<footer class="container-fluid py-4 text-light text-center footer-personalizado">
  <div class="container">
  <div class="iconos-fijos">
    <div class="iconos-separacion content-telefono">
       <a href="tel:44 2119 1346" target="_blank">
        <img class="telefono-movil" src="<?php  bloginfo('template_url'); ?>/images/telefono.png" alt="telefono contacto">
      </a>
    </div>
    <div class="iconos-separacion">
      <a href="https://wa.link/97xwel" target="_blank">
       <img src="<?php  bloginfo('template_url'); ?>/images/whats-flotante.png" alt="icono de contacto whats">
      </a>
    </div>
  </div>
 <img class="icono-footer" src="<?php  bloginfo('template_url'); ?>/images/icono-footer.png" alt="icono consultor"> 
  <div class="contacto-footer">
    <a class="text-contacto" href="https://wa.link/97xwel">
       <img class="icono-whats" src="<?php  bloginfo('template_url'); ?>/images/footer-whats.png" alt="icono whatsapp footer">
       44 2119 1346
    </a>
    <a class="text-contacto" href="mailto:customercare@elvendedorconsultor.com">
      <img class="icono-correo" src="<?php  bloginfo('template_url'); ?>/images/correo-icon.png" alt="icono correo">
      customercare@elvendedorconsultor.com
    </a>
  </div>
  <div class="redes-sociales">
    <a href="https://www.linkedin.com/in/oscar-albarran" class="icono-linkedin">
    <img  src="<?php  bloginfo('template_url'); ?>/images/linkedin.svg" alt="linkedin footer">
    </a>
    <a class="icono-telefono" href="tel:44 2119 1346" target="_blank">
      <img src="<?php  bloginfo('template_url'); ?>/images/telefono.svg" alt="telefono contacto">
    </a>
    <a href="#" class="icono-facebook" >
    <img src="<?php  bloginfo('template_url'); ?>/images/facebook.svg" alt="facebook footer">
    </a>
  </div>
  <p class="derechos-reservados">
    ® El vendedor consultor 2020. Todos los derechos reservados.
  </p>
</div>
</footer>
    <div id="miModal" class="modalcontacto">
      <div class="flexible" id="flex">
        <div class="contenido-modal">
          <div class="flexible modal-cabecera">
             <h1 class="title-contacto">
               Contacto
             </h1>
             <span class="close" id="close">
               &times;
             </span>
             <div class="modal-body">
              <p class="contacto-text">
                Agendemos una primera reunión de 30 minutos, presencial o virtual,
                para entender más y mejor tu negocio y tus intereses, objetivos,
                preocupaciones, para definir si te podemos ayudar y bajo qué esquema de colaboración.
              </p>
               <p class="contactame">
                 Contáctanos por:
               </p>
               <div class="contacto-modal">
                 <a class="text-contacto" href="">
                   <img class="icono-whats" src="<?php  bloginfo('template_url'); ?>/images/footer-whats.png" alt="icono whatsapp footer">
                   44 2119 1346
                 </a>
                 <a class="text-contacto" href="mailto:customercare@elvendedorconsultor.com">
                   <img class="icono-correo" src="<?php  bloginfo('template_url'); ?>/images/correo-icon.png" alt="icono correo">
                   customercare@elvendedorconsultor.com
                 </a>
               </div>
               <p class="tienes-dudas">
                 Déjame tus datos y me pondré en contacto lo antes posible:
               </p>

               <div class="formulario-modal">
                 
                 <?php
                  /** dev.elvendedorconsultor.com */
                  echo do_shortcode( '[contact-form-7 id="122" title="contacto vendedor"]');
                  
                  /** local */
                  // echo do_shortcode( '[contact-form-7 id="86" title="contacto vendedor"]');
                 ?>
               </div>
             </div>
          </div>
        </div>
      </div>
    </div>

   <?php wp_footer(); ?>

   
<script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/6.4.8/swiper-bundle.min.js"></script>

<script type="text/javascript">

document.addEventListener('DOMContentLoaded', function () {
   new Splide('.splide').mount();
});


// let flexible = document.getElementById('flexible');
// usando jquery

$('#menu-mi-menu li:last-child, .boton-contacto').on('click',function(){
  $('#miModal').fadeIn();
});

$('#close').on('click',function(){
  $('#miModal').fadeOut();
});

function initParadoxWay() {
    "use strict";
   
    if ($(".testimonials-carousel").length > 0) {
        var j2 = new Swiper(".testimonials-carousel .swiper-container", {
            preloadImages: false,
            slidesPerView: 1,
            spaceBetween: 20,
            loop: true,
            grabCursor: true,
            mousewheel: false,
            centeredSlides: true,
            pagination: {
                el: '.tc-pagination',
                clickable: true,
                dynamicBullets: true,
            },
            navigation: {
                nextEl: '.listing-carousel-button-next',
                prevEl: '.listing-carousel-button-prev',
            },
            breakpoints: {
                1024: {
                    slidesPerView: 3,
                },
                
            }
        });
    }
    
// bubbles -----------------
    
/*    
    setInterval(function () {
        var size = randomValue(sArray);
        $('.bubbles').append('<div class="individual-bubble" style="left: ' + randomValue(sArray) + 'px; width: ' + size + 'px; height:' + size + 'px;"></div>');
        $('.individual-bubble').animate({
            'bottom': '100%',
            'opacity': '-=0.7'
        }, 4000, function () {
            $(this).remove()
        });
    }, 350); */
    
}

//   Init All ------------------
$(document).ready(function () {
    initParadoxWay();
});
</script>

<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';fnames[5]='BIRTHDAY';ftypes[5]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
</body>  
</html>